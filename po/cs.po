# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Pavel Borecki <pavel.borecki@gmail.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-10 12:53+0100\n"
"PO-Revision-Date: 2022-02-10 11:53+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Czech (http://www.transifex.com/xfce/xfce-panel-plugins/language/cs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: ../panel-plugin/config_gui.c:96
msgid "Device"
msgstr "Zařízení"

#: ../panel-plugin/config_gui.c:109
msgid "Input the device name, then press <Enter>"
msgstr "Zadejte název zařízení a stiskněte <Enter>"

#: ../panel-plugin/config_gui.c:111
msgid "/dev/sda1"
msgstr "/dev/sda1"

#: ../panel-plugin/config_gui.c:124
msgid "Data collection period"
msgstr "Interval pro sběr dat"

#: ../panel-plugin/config_gui.c:127
msgid "Update interval (s) "
msgstr "Interval obnovení (s)"

#: ../panel-plugin/config_gui.c:133
msgid "Label"
msgstr "Popisek"

#: ../panel-plugin/config_gui.c:136
msgid "Tick to display label"
msgstr "Kliknutím zobrazíte popisek"

#: ../panel-plugin/config_gui.c:141
msgid "Input the label, then press <Enter>"
msgstr "Zadejte popisek a stiskněte <Enter>"

#: ../panel-plugin/config_gui.c:143
msgid "hda1"
msgstr "hda1"

#: ../panel-plugin/config_gui.c:151
msgid "Monitor"
msgstr "Sledování"

#: ../panel-plugin/config_gui.c:157
msgid "I/O transfer"
msgstr "Přenos V/V"

#: ../panel-plugin/config_gui.c:160
msgid "MiB transferred / second"
msgstr "MiB přeneseno za sekundu"

#: ../panel-plugin/config_gui.c:164
msgid "Busy time"
msgstr "Aktivita"

#: ../panel-plugin/config_gui.c:167
msgid "Percentage of time the device is busy"
msgstr "Procentuální podíl aktivity zařízení"

#: ../panel-plugin/config_gui.c:175
msgid "Max. I/O rate (MiB/s) "
msgstr "Nejvyšší přenosová rychlost V/V (MiB/s) "

#: ../panel-plugin/config_gui.c:189
msgid "Input the maximum I/O transfer rate of the device, then press <Enter>"
msgstr "Zadejte maximální V/V přenosovou rychlost zařízení a stiskněte <Enter>"

#: ../panel-plugin/config_gui.c:191
msgid "35"
msgstr "35"

#: ../panel-plugin/config_gui.c:199
msgid "Combine Read/Write data"
msgstr "Čtení a zápis dat najednou"

#: ../panel-plugin/config_gui.c:202
msgid "Combine Read/Write data into one single monitor?"
msgstr "Spojit sledování přečtených a zapsaných dat?"

#: ../panel-plugin/config_gui.c:210
msgid "Bar color "
msgstr "Barva panelu"

#: ../panel-plugin/config_gui.c:222 ../panel-plugin/config_gui.c:271
#: ../panel-plugin/config_gui.c:277
msgid "Press to change color"
msgstr "Pro změnu barvy klikněte sem"

#: ../panel-plugin/config_gui.c:231
msgid "Read bar color "
msgstr "Barva panelu čtení"

#: ../panel-plugin/config_gui.c:240
msgid "Write bar color "
msgstr "Barva panelu zápisu"

#: ../panel-plugin/config_gui.c:247
msgid "Bar order"
msgstr "Pořadí panelů"

#: ../panel-plugin/config_gui.c:254
msgid "Read-Write"
msgstr "Čtení-zápis"

#: ../panel-plugin/config_gui.c:257
msgid "\"Read\" monitor first"
msgstr "Sledovat „Čtení“ jako první"

#: ../panel-plugin/config_gui.c:261
msgid "Write-Read"
msgstr "Zápis-čtení"

#: ../panel-plugin/config_gui.c:264
msgid "\"Write\" monitor first"
msgstr "Sledovat „Zápis“ jako první"

#: ../panel-plugin/main.c:188
#, c-format
msgid "%s: Device statistics unavailable."
msgstr "%s: Statistika o zařízení není dostupná."

#: ../panel-plugin/main.c:225
#, c-format
msgid ""
"%s\n"
"----------------\n"
"I/O (MiB/s)\n"
"  Read: %3.2f\n"
"  Write: %3.2f\n"
"  Total: %3.2f\n"
"Busy time (%c)\n"
"  Read: %d\n"
"  Write: %d\n"
"  Total: %d"
msgstr ""

#: ../panel-plugin/main.c:829
#, c-format
msgid ""
"%s\n"
"%s: %s (%d)\n"
"\n"
"This monitor will not work!\n"
"Please remove it."
msgstr "%s\n%s: %s (%d)\n\nToto sledování nebude pracovat!\nOdeberte jej."

#: ../panel-plugin/main.c:840
#, c-format
msgid ""
"%s: No disk extended statistics found!\n"
"Either old kernel (< 2.4.20) or not\n"
"compiled with CONFIG_BLK_STATS turned on.\n"
"\n"
"This monitor will not work!\n"
"Please remove it."
msgstr "%s: Nenalezeny žádné podrobné statistiky o disku!\nPoužíváte zastaralé jádro (< 2.4.20) nebo jste\nnezapnuli volbu CONFIG_BLK_STATS při sestavování.\n\nToto sledování nebude pracovat!\nOdeberte jej."

#: ../panel-plugin/main.c:848
#, c-format
msgid ""
"%s: Unknown error\n"
"\n"
"This monitor will not work!\n"
"Please remove it."
msgstr "%s: Neznámá chyba\n\nToto sledování nebude pracovat!\nOdeberte jej."

#: ../panel-plugin/main.c:871
msgid ""
"Diskperf monitor displays instantaneous disk I/O transfer rates and busy "
"times"
msgstr "Monitorování výkonu disku okamžitě zobrazuje aktuální přenosové rychlosti vstupně výstupních operací a vytížení."

#: ../panel-plugin/main.c:873
msgid "Copyright (c) 2003, 2004 Roger Seguin"
msgstr "Copyright (c) 2003, 2004 Roger Seguin"

#: ../panel-plugin/main.c:906 ../panel-plugin/diskperf.desktop.in.h:1
msgid "Disk Performance Monitor"
msgstr "Monitorování výkonu disku"

#: ../panel-plugin/diskperf.desktop.in.h:2
msgid "Show disk performance"
msgstr "Zobrazuje výkon disku"
